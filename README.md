# Medtronic MiniMed 780G Website

## Project tech:

- [Vue.js](https://vuejs.org/), the JavaScript framework
- [Gridsome](https://gridsome.org/), a static site generator based on Vue.js
- [BootstrapVue](https://bootstrap-vue.js.org/) as the front-end CSS library
- [vue-fontawesome](https://github.com/FortAwesome/vue-fontawesome) for Font Awesome icons

## Project Install

Start by cloning the repo. To edit this project on your computer, you must also have Nove v8.0 installed, and NPM (which comes with Node).

```
npm install
```

## Project development preview

```
gridsome develop
```

## Project build for production

This builds the site into static files for deployment.

```
gridsome build
```

## Project deployment (for casual previews only, not final deployment)

This uses the free Starter tier of [netlify](https://www.netlify.com/).

The URL for this deployment is [https://minimed-780g.netlify.com](https://minimed-780g.netlify.com).

```
TBD
```
