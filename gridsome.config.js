module.exports = {
  siteName: 'Medtronic MiniMed 780G',
  titleTemplate: '%s | Medtronic MiniMed 780G',
  pathPrefix: '/medtronic/minimed_780g/prototype/v01',
  siteUrl: 'https://demo.fredcomm.com',
  siteDescription: 'Protocol for Advanced Hybrid Closed Loop Therapy: MiniMed™ 780G System',
  plugins: []
}