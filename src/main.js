// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from "~/layouts/Default.vue";

// Bootstrap
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Font Awesome
import {
  FontAwesomeIcon
} from "@fortawesome/vue-fontawesome";
import {
  config,
  library
} from "@fortawesome/fontawesome-svg-core";
import {
  faGithub,
  faTwitter
} from "@fortawesome/free-brands-svg-icons";
import {
  faVideo,
  faArrowRight,
  faArrowCircleRight,
  faChevronRight
} from "@fortawesome/free-solid-svg-icons";
import "@fortawesome/fontawesome-svg-core/styles.css";
config.autoAddCss = false;
library.add(faGithub, faTwitter, faVideo, faArrowRight, faArrowCircleRight, faChevronRight);

// Custom global styles, including Bootstrap updates
import "~/assets/css/custom.scss";

export default function (Vue, {
  router,
  head,
  isClient
}) {
  Vue.component("Layout", DefaultLayout);
  Vue.use(BootstrapVue);
  Vue.component("font-awesome", FontAwesomeIcon);
}